#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
# @Date    : 2020-12-07 14:01:35
# @Author  : Shen Chucheng (chuchengshen@fuzhi.ai)
# @Desc    : 


from os import path
from setuptools import setup, find_packages

try:
    pkg_name = 'gbclient'
    libinfo_py = path.join(pkg_name, '__init__.py')
    with open(libinfo_py, 'r', encoding='utf8') as f:
        for line in f:
            if line.startswith('__version__'):
                exec(line)
                break
    exec(line)  # gives __version__
    assert bool( __version__ )
except:
    __version__ = '0.0.0'
try:
    with open('requirements.txt', encoding='utf8') as f:
        _install_requires = f.read().splitlines()
except FileNotFoundError:
    _install_requires = []

try:
    with open('README.md', encoding='utf8') as f:
        _long_description = f.read()
except FileNotFoundError:
    _long_description = ''

_description = '{} is a RPC Client for MetaCenter Server to make this thrift ' \
        'rpc application easy to use'.format(pkg_name.capitalize())

setup(
    name=pkg_name,
    version=__version__,
    packages=find_packages(),
    description=_description,    
    long_description=_long_description,
    long_description_content_type='text/markdown',
    license='Apache 2.0',
    install_requires=_install_requires,
    package_data={"": ["*.thrift"]},
    author="Shen Chucheng",
    author_email="chuchengshen@fuzhi.ai",
)

