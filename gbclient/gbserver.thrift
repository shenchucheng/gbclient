include "thriftrpc/exceptions/exceptions.thrift"

service GBService{
    string schedule (
        1: required string input_json,
    ) throws (
        1: exceptions.FeaturesNotReadyError fnferror
    )
}
