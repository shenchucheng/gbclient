#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
# @Date    : 2021-02-07 17:57:09
# @Author  : Shen Chucheng (chuchengshen@fuzhi.ai)
# @Desc    : GBCLIENT


__version__ = "0.0.1"


from os.path import dirname, join
from thriftrpc.utils.rpc import load

_dir = dirname(__file__)
gbserver = load(join(_dir, "gbserver.thrift"))

exceptions = gbserver.exceptions
GBService = gbserver.GBService


__all__ = [
   "exceptions", "GBService",
]

